# Welcome to ArmA3 Script Library

 A place for hosting scripts useful to ArmA3 Zeus mission makers.

 ## Documentation guide
 To ensure easy script usage the following standards will be used:

 1. Each script will be contained in it's own **single .sqf file**, unless the scripts are very closely related (like an on/off script for a thing). Closely related scripts in a single file **will be separated by**:
 
```
// ==================================================
```
 
 2. The Script filename will be descriptive but concise, starting with a prefix:
 
 *eden_* = denotates that the code Script code is to be set up in the **3DEN Editor** or in the mission files.
 *zeus_* = denotates that the code Script code is to be used during a live **Zeus Session**.

 3. Each script will have the following header:

```
// <EXEC_LOCATION - EXEC_LOCALITY || SCRIPT_FILE>
// <Short description of the way the script is used and what it does. Can include an example.>
```

EXEC_LOCATION values:

 - DBC = Script should be copied into the **Debug Console**
 - XCM = Script should be copied into the **Achilles Execute Code Module**
   - YOU **MUST NOT COPY THE COMMENTS** WHEN USING XCM!!!

EXEC_LOCALITY values:

 - SERVER = Script should be executed on the **Server Only**
 - GLOBAL = Script should be executed **On Every Machine**
 - LOCAL = Script should be executed **On Local Machine Only**

SCRIPT_FILE can be:

 - **Description.ext** - the standard config file for missions.
 - An **Event Script File** - the mission maker needs to use the Script code in a corresponding [Event Script File defined by the ArmA3 engine](https://community.bistudio.com/wiki/Event_Scripts).
 
Keep in mind that you can also use these by putting them into a separate **.hpp** file and including them in other files with [#include](https://community.bistudio.com/wiki/PreProcessor_Commands#.23include).

 - Can also be other locations such as **OBJECT INIT**, the 3den's object init box.

#### Examples:

>zeus_resupply_box.sqf

    // XCM - SERVER
    // Place the module on a container object to clear it's cargo and add the magazines that the players use to it instead.

 4. Scripts can contain some variables to enable the user to modify it's behavior. Such variables, if present, will be separated from the rest of the script by 3 empty lines.

## Other projects

[ArmA3 Radiation/Geiger Mechanics](https://gitlab.com/mariusgaming/marius_geiger/)

## Credits
Repository initiated by [Marius](https://ko-fi.com/arma3marius) for [Mutiny Gaming](https://discord.gg/G4kH3sT).