// DBC - SERVER
// New arty fnc with low angle.

params [
["_targetGRID", ""],			// ignored if POLAR, otherwise GRID (can be any size)
["_rounds", 1],
["_adjustFireOneGun", 0],		// if 1 only one gun will fire
["_angle", 0],					// if 0 use low angle

["_left_right", 0],				// positive = right
["_drop_add", 0],				// positive = add

["_observerPos", getPos player], // position form which POLAR will be calculated
["_polarDir", 0],				// azimuth when using POLAR
["_polarRng", 0],				// range when using POLAR; set to 0 to use GRID

["_batteryGroups", [a1]],		// input list of global grp names or object names here
["_magazine", ""],				// 32Rnd_155mm_Mo_shells  2Rnd_155mm_Mo_LG  |  8Rnd_82mm_Mo_shells
["_reloadBattery", true],		// reload the entire object before firemission
["_sequenceDelay", 0.5]			// delay of firing between guns in a single volley
];


// artillery piece parameters
_artyCharge = ["Single5", "Single4", "Single3", "Single2", "Single1"];

// NATO/CSAT 155mm (Scorcher/Sholef), RHS M109A6
_artyRoundVelocity = [810.0, 648.0, 388.8, 243.0, 153.9];
_artyMaxRanges = [66903, 42818, 15414, 6021, 2415];


// experimental factors - accounting for air resistance probably
_LowAngleFactor = [1,0.995,0.99,0.975,0.95];

// compose array of units from _batteryGroups
_batteryGroup = [];
{
	if (isNil {_x}) then {
		["ERR0 Unit type declared is nil!"] remoteExecCall ["systemChat", allCurators + [2]];
		terminate _thisScript;
	} else {
		_batteryGroup append (units _x)};
} forEach _batteryGroups;

// GRID MISSION
if (_polarRng isEqualTo 0) then {

	_gridSize = count _targetGRID;
	_targetGRIDX = _targetGRID select [0, _gridSize/2];
	_targetGRIDY = _targetGRID select [_gridSize/2];

	// append 0s, because the way game coords work
	while {count _targetGRIDX < 5} do {_targetGRIDX = _targetGRIDX + "0"};
	while {count _targetGRIDY < 5} do {_targetGRIDY = _targetGRIDY + "0"};

	_targetGRID = parseSimpleArray ("[" + _targetGRIDX + "," + _targetGRIDY  + ",0]");

	// handle adjustment of coords
	if (_left_right != 0 || _drop_add != 0) then {
		_targetGRID = [(_targetGRID param [0]) + _left_right, (_targetGRID param [1]) + _drop_add, 0];
	};

// POLAR PLOT MISSION
} else {

	_targetGRID = _observerPos getPos [_polarRng + _drop_add, _polarDir]; // get inital polar plot and calculate in add/drop adjustment

	// handle adjustment of left right coords if needed
	if (_left_right != 0) then {
		_targetGRID = _targetGRID getPos [_left_right, _polarDir + 90];
	};

	_targetGRID set [2,0]; // z component to 0

};

// declare an array of arty guns to fire
_gunsArray = [];
{
	// handle ship guns:
	if (_x isKindOf "B_Ship_Gun_01_base_F") then {_gunsArray pushBack _x} else {
		_gunsArray pushBackUnique objectParent _x;
	};
} forEach _batteryGroup;
_gunsArray = _gunsArray - [ObjNull]; // null obj are returned for infantry so we yeet them

if (_adjustFireOneGun != 0) then {
	_gunsArray resize 1;
};

if (_reloadBattery) then {
	{
		[_x, 1] remoteExecCall ["setVehicleAmmoDef", _x];
	} forEach _gunsArray;
};

// handle default magazine
if !(_magazine in getArtilleryAmmo _gunsArray) then {
	_magazine = (getArtilleryAmmo _gunsArray) select 0; // may get assigned nil if guns are empty
};

// if arty has no shells remaining the _magazine will be nil because getArtilleryAmmo will be empty []
if (isNil "_magazine") exitWith {
	["ERR1 Artillery has no ammo!"] remoteExecCall ["systemChat", allCurators + [2]];
	[_targetGRID, "noammo!"];
};

// check if in range and return message
_artyETA = _gunsArray#0 getArtilleryETA [_targetGRID, _magazine]; // -1 if not in range

// arty calculation:
private ["_v","_charge","_gunElevation","_vfactor"];

// 1. pick proper velocity/charge for the given range
_range = _gunsArray#0 distance2D _targetGRID;
{
	if (_range < _x) then {
		_v = _artyRoundVelocity select _forEachIndex;
		_charge = _artyCharge select _forEachIndex;
		_vfactor = _LowAngleFactor select _forEachIndex;
	};
} forEach _artyMaxRanges;

// 2. find theta angle (y is the vertical component in this context)
_yOfArtillery = (getPosASL (_gunsArray#0) select 2);
_g = 9.8; // gravity factor
_ydiff = ((AGLToASL _targetGRID) select 2) - _yOfArtillery; // find difference of height
if (_angle isEqualTo 0) then {
	_gunElevation = _vfactor * atan((_v^2 - sqrt(_v^4 - _g * (_g * _range^2 + 2 * _ydiff * _v^2))) / (_g * _range));
} else {
	_gunElevation = atan((_v^2 + sqrt(_v^4 - _g * (_g * _range^2 + 2 * _ydiff * _v^2))) / (_g * _range));
};

if !(_gunElevation isEqualType 0) exitWith {
	["ERR3 atan function failed!"] remoteExecCall ["systemChat", allCurators + [2]];
	[_gunElevation, "bad atan!"];
};

// 3. calculate a position at which to aim the gun at altitude above target so the gun has the correct angle of elevation
_gunAimpointAltitudeASL = (tan(_gunElevation)) * _range;

// aim the gun at correct angle;
// we add the _yOfArtillery to z to account for the fact that Arty is probably not at sea level
{
	[_x, [[0], weapons _x select 0, _magazine]] remoteExecCall ["loadMagazine", _x];
	[(gunner _x), [_targetGRID select 0, _targetGRID select 1, _yOfArtillery + _gunAimpointAltitudeASL]] remoteExecCall ["doWatch", _x];
} forEach _gunsArray; // aim all the guns

// prep for finding a 0 based index in the forEach below
reverse _artyCharge;

// fire guns function - delays less than ~14 sec seem to cause problems
{

	[[_x, _magazine, _charge, _rounds, _artyCharge, _forEachIndex * _sequenceDelay], {

		params["_artyGun","_magazine", "_charge", "_rounds", "_artyCharge", "_delay"];

		sleep 8; // wait for aim & magazine reload
		sleep _delay; // delay for sequential firing

		// prevent AI from twitching the gun (seems to be a bug when using "fire")
		(gunner _artyGun) disableAI "WEAPONAIM";
		(driver _artyGun) disableAI "PATH";

		_index = _artyCharge find _charge;
		_artyGun action ["SWITCHWEAPON", _artyGun, gunner _artyGun, _index];
		sleep 0.1; // a slight delay is need to switch weapons

		for "_i" from 1 to _rounds do {
			_artyGun fireAtTarget [objNull];
			if (_i != _rounds) then {
				sleep 14; // fire loop bugs often with less delay than ~14 sec
			};
		};

		// give back AI for next mission
		(gunner _artyGun) enableAI "WEAPONAIM";
		//(gunner _artyGun) doWatch objNull; // reset barrel pointing
		(driver _artyGun) enableAI "PATH";

	}] remoteExecCall ["spawn", _x];

} forEach _gunsArray;

// console return
[_targetGRID, _rounds, _gunElevation, _charge];