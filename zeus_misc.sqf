BI ARSENAL
==========
["AmmoboxInit",[this,true]] call BIS_fnc_arsenal;


ACE ARSENAL:
============
[this, true] call ace_arsenal_fnc_initBox;


GET ARSENAL CLASSES
===================
copyToClipboard str(uiNamespace getVariable "ace_arsenal_configItems");


GET ARSENAL ITEMS FROM UNITS
============================
private _items = allUnits apply {getUnitLoadout _x};
_items = str _items splitString "[]," joinString ",";
_items = parseSimpleArray ("[" + _items + "]");
_items = _items arrayIntersect _items select {_x isEqualType "" && {_x != ""}};
copyToClipboard str _items;


GET ARSENAL ITEMS FROM A TRUCK CARGO
====================================
private _items = []; 
{_items pushBackUnique _x} forEach (getWeaponCargo truck select 0); 
{_items pushBackUnique _x} forEach (getMagazineCargo truck select 0); 
{_items pushBackUnique _x} forEach (getItemCargo truck select 0); 
{_items pushBackUnique _x} forEach (getBackpackCargo truck select 0); 
copyToClipboard _items;
items;


CROSS OUT ORBAT
===============
[missionConfigFile >> "CfgOrbat" >> "mg", [1,0,0,1], 1.2,1.2, 45] call BIS_fnc_ORBATAddGroupOverlay;


FIX RESPAWN VEHICLE MODULE EXPLODING (EXPRESSION)
=================================================
(_this#0) enableSimulationGlobal false; (_this#0) setVehiclePosition [_this#0, [], 0]; _this#0 spawn {sleep 0.1; _this enableSimulationGlobal true};

BAR GATE
========
yourbargatename animate ["Door_1_rot", 1]; //this will open the bargate


TRACE BULLETS
=============
[player,1] spawn BIS_fnc_traceBullets;


TRACK A PLAYER WITH MARKER
==========================
tracker = _this select 1 spawn {
	_leader = _this;
	_markerName = createMarker ["leaderpos", _leader];
	"leaderpos" setMarkerType "b_hq";
	"leaderpos" setMarkerColor "ColorGUER";
	"leaderpos" setMarkerSize [0.8,0.8];
	while {true} do {
		sleep 30;
		"leaderpos" setMarkerPos (getPos _leader);
	};
};

LAMP SWITCH
===========
_lamps = nearestObjects [player, ["House","Lamps_base_F", "PowerLines_base_F", "PowerLines_Small_base_F"], 2000];
{_x switchLight "off"} forEach _lamps;


NO MOVE AI
==========
_grp = units group (_this select 1);
{_x disableAI "path"} forEach _grp;


HIDE TREES & BUSHES
===================
{ _x hideObjectGlobal true } foreach (nearestTerrainObjects [_this select 0,["tree","bush"],50])


ATTACH STROBE BLUFOR
====================
_veh = "B_IRStrobe" createVehicle [0,0];
_veh attachTo [_unit, [0,-0.1,-0.04], "neck"];


BULLETCAM
=========
_this # 1 addEventHandler ["Fired", {
	if (_this select 7 == player) then {
		_null = _this spawn {
			_missile = _this select 6;			
			_cam = "camera" camCreate [0,0];
			_cam camSetTarget _missile;
			_cam cameraEffect ["External", "Back"];
			while {!isNull _missile} do {
				_cam camSetRelPos [0,-3,0];
				_cam camCommit 0;
			};
			sleep 2;
			_cam cameraEffect ["Terminate", "Back"];
			camDestroy _cam;
		};
	};
}];

_this # 1 removeAllEventHandlers "Fired";

SHELLCAM
========
this addEventHandler ["Fired", {
	_null = _this spawn {
		_missile = _this select 6;
		_cam = "camera" camCreate [0,0];
		_cam camSetTarget _missile;
		_cam cameraEffect ["External", "Back"];
		while {!isNull _missile} do {
			_pos = (getPos _missile);
			_pos params ["_xx","_yy","_zz"];
			_cam camSetPos [_xx,_yy,_zz+10];
			_cam camCommit 0;
		};
		sleep 3;
		_cam cameraEffect ["Terminate", "Back"];
		camDestroy _cam;
	};
}];


TYPE TEXT
=========

_day = date #2;
_month = date #1;
_year = date #0;
_hrs = format["%1", date select 3];
if (date select 3<10) then {_hrs = "0" + _hrs};
_min = format["%1", date select 4];
if (date select 4<10) then {_min = "0" + _min};
[
	[
		["US camp 'Nui Kang Se', Central Highlands","align='right' font='PuristaMedium' shadow='2'"],["","<br/>"],
		["Mutiny SF Group","align='right' font='PuristaMedium' shadow='2'"],["","<br/>"],
		[format["%1-%2-%3 %4%5", _day, _month, _year, _hrs, _min],"align='right' font='PuristaMedium' shadow='2'"]
	],
	(safezoneXAbs - (0.02 * safezoneW)), (safezoneH * 0.6), false
] spawn BIS_fnc_typeText2;


SPAWN UAV CUSTOM NON DRIVABLE
=============================

_altitude = 1200;
_pos = [4088,4596];
_veh = "B_UAV_02_F" createVehicle _pos;
createVehicleCrew _veh;
_veh lockDriver true;
_veh enableUAVWaypoints false;
_way = (group driver _veh) addWaypoint [_pos, 0];
_way setWaypointVisible false;
_way setWaypointType "LOITER";
_way setWaypointLoiterRadius 1200;
[_veh, _altitude] spawn {
	params["_drone","_alt"];
	while {canMove _drone} do {
		if (isAutonomous _drone) then {
			_drone setAutonomous false;
			_drone flyInHeightASL [_alt,_alt,_alt];
			};
		sleep 0.05;
	};
};
sleep 3;
getPos _veh params["_x","_y","_z"];
_veh setPos [_x, _y, _z + _altitude];
_veh setVelocityModelSpace [0, 60, 0];
sleep 1;
_veh flyInHeightASL [_altitude, _altitude, _altitude];


SPAWN AC-130
============

_pos = [1544.61,4960.49,0];
_veh = "RHS_C130J" createVehicle _pos;
_turret = "B_Heli_Attack_01_F" createVehicle [_pos select 0, (_pos select 1) + 50];
_turret lockDriver true;
_turret enableCopilot false;
_turret attachTo [_veh,[-0.0192871,1.0166,2.4]];
_turret setDir 180;

{_turret removeWeapon _x} forEach weapons _turret;

_turret addMagazines ["4000Rnd_20mm_Tracer_Red_shells",2];
_turret addWeapon "gatling_20mm_VTOL_01";

_turret addMagazines ["12Rnd_125mm_HE_T_Red",2];
_turret addWeapon "cannon_125mm";

_turret addMagazines ["240Rnd_40mm_GPR_Tracer_Red_shells",2];
_turret addWeapon "autocannon_40mm_VTOL_01";

======= OLD WITH

_pos = [1536.83,4968.52];
_veh = "RHS_C130J" createVehicle _pos;
_turret = "B_AAA_System_01_F" createVehicle [_pos select 0, (_pos select 1) + 50];
createVehicleCrew _turret;
_turret attachTo [_veh,[-2.55811,-4,2.8]];
_turret setDir 270;
_turret setVectorUp [-0.2,0,0.5];

_turret addMagazines ["12Rnd_125mm_HE_T_Red",2];
_turret addWeapon "cannon_125mm";

_turret addMagazines ["240Rnd_40mm_GPR_Tracer_Red_shells",2];
_turret addWeapon "autocannon_40mm_VTOL_01";


REMOVE WEAPON
=============

{
	if (typeOf _x == "B_MBT_01_arty_F") then {
		_x removeWeaponGlobal "HMG_127_APC";
		_x removeWeaponGlobal "GMG_40mm";
	};
} forEach vehicles;


ADD MG
======
_this #1 addMagazines ["130Rnd_338_Mag",2];
_this #1 addWeapon "MMG_02_vehicle";


AI SKILL
========
{_x setSkill 0} forEach allUnits;

MINE
====
_mine = createMine ["underwatermine", (player getPos [7, getDir player]), [], 0];
(getPosASL _mine) params ["_xx","_yy"];
_distance = getPosASL _mine distance getPosATL _mine;
_mine setPosATL [_xx,_yy, _distance/2 - 45.6387];