// XCM - SERVER
// Place the module on a container object to clear it's cargo and add the magazines that the players use to it instead.

_numMags = 30;
_addGrenades = true;
_debugText = false;



_cargo = _this # 1;
clearWeaponCargoGlobal _cargo;
clearMagazineCargoGlobal _cargo;
clearItemCargoGlobal _cargo;
clearBackpackCargoGlobal _cargo;

_magArray = [];
{
	_magArray pushBackUnique (primaryWeaponMagazine _x # 0);
	if (count primaryWeaponMagazine _x == 2 && _addGrenades) then {
		_magArray pushBackUnique (primaryWeaponMagazine _x # 1);
	};
	_magArray pushBackUnique (handgunMagazine _x # 0);
	_magArray pushBackUnique (secondaryWeaponMagazine _x # 0);
	if _addGrenades then {
		{
			_magArray pushBackUnique _x;
		} forEach magazines _x;
	};
} forEach allPlayers - entities "HeadlessClient_F";

{_cargo addMagazineCargoGlobal [_x, _numMags]} forEach _magArray;

if _debugText then {
	_message = format["Successfully added %1 to %2", _magArray, _cargo];
	{[_message] remoteExec ["systemChat", _x];} forEach allCurators;
};