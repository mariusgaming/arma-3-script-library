// DBC - SERVER
// Use this to simulate an emp that will disable ALL vehicles and most player electronics and HUD
_emp = [] spawn {
	[4, BIS_fnc_earthquake] remoteExec ["call"];

	sleep 4;

	{
	_x unassignItem "ItemRadio";
	_x removeItems "ItemRadio";
	_x unassignItem "ItemGPS";
	_x removeItems "ItemGPS";
	_x unassignItem "ACE_microDAGR";
	_x removeItems "ACE_microDAGR"} forEach allUnits;

	_killVicsAndHUD = {
		{if (local _x) then {_x setFuel 0}} forEach vehicles;
		showHUD [true, false, false, false, false, false, false, true, false];
	};

	[[], _killVicsAndHUD] remoteExec ["spawn"];
	
	// kill TFAR radios except for server
	[[], {player setVariable ["tf_unable_to_use_radio", true]}] remoteExec ["call", -2];

	// apply it after respawn as well, and for JIPs
	[[], {player addEventHandler ["Respawn", {
				[] spawn {
				sleep 1;
				showHUD [true, false, false, false, false, false, false, true, false];
				player setVariable ["tf_unable_to_use_radio", true];
				}
			}
		]
	}] remoteExec ["call", -2, true];
};


// =========================================================================

// XCM - SERVER
// reverse emp, but only for current players, will not work on respawned units for now
[[], {player setVariable ["tf_unable_to_use_radio", false];
showHUD [true, true, true, true, true, true, true, true, true]}] remoteExec ["call", _this#1];